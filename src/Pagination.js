import React from 'react'
import './Pagination.css'

const Pagination = ({ postPerPage, totalPosts, paginate }) => {
    const pageNumber = [];

    for (let i = 1; i <= Math.ceil(totalPosts / postPerPage); i++) {
        pageNumber.push(i)
    }

    return (
        <nav className='pagination-nav'>
            <ul className='pagination'>
                {pageNumber.map(number => (
                    <p key={number} className='page-item'>
                        <a onClick={()=> paginate(number)} href='!#' className='page-link'>
                            {number}
                        </a>
                    </p>
                ))}
          
            </ul>
        </nav>
       


    )
}
export default Pagination;
