import React, { useState, useEffect } from 'react';
//import logo from './logo.svg';
import './App.css';
import { Pagination } from "antd";
import Modal from 'react-modal'

//Custom Components
import Header from './Header';
//import Pagination from './Pagination';
import * as API from './Apis'

Modal.setAppElement('#root')




function App() {
  const [charecterOrder, setCharecterOrder] = useState('');
  const [charecterRace, setCharecterRace] = useState('');
  const [charecterGender, setCharecterGender] = useState('');
  const [characterDataSourse, setCharacterDataSourse] = useState([]);
  const [copyCharecterDataSourse, setCopyCharecterDataSourse] = useState([]);
  const [modifiedDataSource, setModifiedDataSource] = useState([])

  //const [posts,setPosts] = useState([])
  const [totalPosts, setTotalPosts] = useState("")
  const [page, setPage] = useState(1);
  const [postPerPage, setPostPerPage] = useState(10)

  const [modalOfCharecter, setMdalOfCharecter] = useState(false);
  const [charecterDetails, setCharecterDetails] = useState('');

  const [searchTerm, setSearchterm] = useState('')


  //const [dataAscending, setDataAscending] = useState()

  useEffect(() => {
    charecterListFunction();
  }, []);

  const charecterListFunction = () => {

    const url = API.CHARECTERS_URL;
    const token = 'rOvpaczKcIYglDVh_wzR'
    console.log("<---------View Booking URL--------->", url);

    fetch(url, {
      method: "GET",
      //mode: "no-cors",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
        Authorization: "Bearer " + token,
      },
    }).then((response) => response.json())
      .then((responseJson) => {
        //console.log(JSON.stringify(responseJson.docs));
        setCharacterDataSourse(responseJson.docs);
        setCopyCharecterDataSourse(responseJson.docs);
        setTotalPosts(responseJson.docs.length);

      })
      //console.log("dataSource",this.state.dataSource)
      .catch((error) => {
        if (error.message === 'Network request failed') {
          console.log('Network request failed');
        }
      })

  }

  const sortOrderChangeFunction = (event) => {
    setCharecterOrder({ charecterOrder: event.target.value });
    if (event.target.value === "ascending") {
      console.log("Ascending Order called")
      const ascendingOrder = characterDataSourse.sort(function (a, b) {
        console.log("Sampleee Acs Name")
        console.log("B Name", b)
        return a.name.localeCompare(b.name)

      });
      setCharacterDataSourse(ascendingOrder)
    }
    else if (event.target.value === "decending") {
      console.log("Decending Order called")
      const desendingOrder = characterDataSourse.sort(function (a, b) {
        console.log("Sampleee Dec Name")
        console.log("A Name", a)
        return b.name.localeCompare(a.name)

      });
      setCharacterDataSourse(desendingOrder)
    }
  }

  const pageSizeFunc = (evt) => {
    console.log("evt", evt.target.value)
    const pageValue = evt.target.value
    setPostPerPage(pageValue);

  }

  //Function for Oder the values by gender

  const genderChangeFunction = (value) => {
    if (value === "male") {

      const filteredData = copyCharecterDataSourse.filter(person => person.gender == 'Male')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (value === "female") {
      const filteredData = copyCharecterDataSourse.filter(person => person.gender == 'Female')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (value === "any") {

      setCharacterDataSourse(copyCharecterDataSourse)
    }


  }

  const callCharecterFunc = () => {
    charecterListFunction()
  }
  const sortOrderByGenderFunction = (evt) => {
    //callCharecterFunc()
    //setCharecterGender({ charecterGender: evt.target.value });
    genderChangeFunction(evt.target.value)
    /* if (evt.target.value === "male") {
       const filteredData = characterDataSourse.filter(person => person.gender == 'Male')
       console.log(filteredData)
       setCharacterDataSourse(filteredData)
     }
     else if (evt.target.value === "female") {
       const filteredData = characterDataSourse.filter(person => person.gender == 'Female')
       console.log(filteredData)
       setCharacterDataSourse(filteredData)
     } */
  }

  const sortOrderByRaceFunc = (evt) => {

    setCharecterRace({ charecterRace: evt.target.value })
    if (evt.target.value === "human") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Human')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "elf") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Elf')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "dwarf") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Dwarf')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "maiar") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Maiar')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "hobit") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Hobbit')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "half-elven") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Half-elven')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "stone-trolls") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Stone-trolls')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "vampire") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Vampire')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "great-eagles") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'Great Eagles')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
    else if (evt.target.value === "Nan") {
      const filteredData = copyCharecterDataSourse.filter(person => person.race == 'NaN')
      console.log(filteredData)
      setCharacterDataSourse(filteredData)
    }
  }



  const modalCharecterOpenFunction = (value) => {
    console.log("Value of charecter", value)
    setCharecterDetails(value);
    setMdalOfCharecter(true)
  }

  /* const ascendingOrderFunction = () => {
     console.log("Ascending Order function called")
     currentPosts.sort(function (a, b) {
       return a - b
     });
   }
   const desendingOrderFunction = () => {
     currentPosts.sort(function (a, b) {
       return a - b
     });
   } */




  const indexOfLastPage = page * postPerPage;
  const indexOfFirstPage = indexOfLastPage - postPerPage;
  //const currentPosts = characterDataSourse.slice(indexOfFirstPage, indexOfLastPage);
  const currentPosts = characterDataSourse.slice(indexOfFirstPage, indexOfLastPage);
  //setModifiedDataSource(characterDataSourse.slice(indexOfFirstPage, indexOfLastPage))

  const onShowSizeChange = (current, pageSize) => {
    setPostPerPage(pageSize)
  }

  const itemRender = (current, type, originalElement) => {
    if (type === "prev") {
      return <a>Previous</a>
    }
    if (type === "next") {
      return <a>Next</a>
    }

    return originalElement
  }

  //Change Page Number Function
  const paginate = (pageNumber) => {
    setPage(pageNumber);
  }

  return (
    <div className="App">
      <Modal
        isOpen={modalOfCharecter}
        onRequestClose={() => setMdalOfCharecter(false)}
        shouldCloseOnOverlayClick={false}
      >


        <div>
          <div className='charecter-header'>
            <h1 className='heading-h1'>Charecter - {charecterDetails.name}</h1>
            <button onClick={() => setMdalOfCharecter(false)} className="closs-btn">Close</button>
          </div>
          <div className='charecter-details-text'>
            <p>{"Name   "}&emsp;&emsp;{charecterDetails.name ? charecterDetails.name : "NAN"}</p>
            <p>{"WikiURL"}&emsp;&emsp; <a href={charecterDetails.wikiUrl}>{charecterDetails.wikiUrl ? charecterDetails.wikiUrl : "NAN"}</a></p>
            <p>{"Race   "}&emsp;&emsp;{charecterDetails.race ? charecterDetails.race : "NAN"}</p>
            <p>{"Gender "}&emsp;&emsp;{charecterDetails.gender ? charecterDetails.gende : "NAN"}</p>
            <p>{"Height "}&emsp;&emsp;{charecterDetails.height ? charecterDetails.height : "NAN"}</p>
            <p>{"Hair   "}&emsp;&emsp;{charecterDetails.hair ? charecterDetails.hair : "NAN"}</p>
            <p>{"Realm  "}&emsp;&emsp;{charecterDetails.realm ? charecterDetails.realm : "NAN"}</p>
            <p>{"Birth  "}&emsp;&emsp;{charecterDetails.birth ? charecterDetails.birth : "NAN"}</p>
            <p>{"Spouse "}&emsp;&emsp;{charecterDetails.spouse ? charecterDetails.spouse : "NAN"}</p>
            <p>{"Death  "}&emsp;&emsp;{charecterDetails.death ? charecterDetails.death : "NAN"}</p>
          </div>
        </div>

      </Modal>
      <Header />
      <div className='top-design'>
        <label className='label-design'>Search Name</label>
        <input type={"text"} id="search_name" placeholder='by name' onChange={event => { setSearchterm(event.target.value) }} />

        <label className='label-design'>Sort By</label>
        <select value={charecterOrder} onChange={sortOrderChangeFunction}>
          <option>Select order</option>
          <option value={"ascending"}>Ascending</option>
          <option value={"decending"}>Descending</option>
        </select>

      </div>

      <div className='top-design'>
        <label className='label-design'>Race</label>
        <select value={charecterRace} onChange={sortOrderByRaceFunc}>
          <option >Select Race Type</option>
          <option value="human">Human</option>
          <option value="elf">Elf</option>
          <option value="dwarf">Dwarf</option>
          <option value="hobit">Hobit</option>
          <option value="maiar">Maiar</option>
          <option value="half-elven">Half-Elven</option>
          <option value="stone-trolls">Stone-Trolls</option>
          <option value="vampire">Vampire</option>
          <option value="great-eagles">Great-Eagles</option>
          <option value="Nan">NAN</option>

        </select>

        <label className='label-design'>Gender</label>
        <select value={charecterGender} onChange={sortOrderByGenderFunction}>
          <option>Select Gender</option>
          <option value={"male"}>Male</option>
          <option value={"female"}>Female</option>
          <option value={"any"}>Any</option>
        </select>

      </div>

      <div className='content-table'>
        <table className='table-main'>
          <thead className='table-head'>
            <tr>
              <th scope='col'>ID</th>
              <th scope='col'>Name</th>
              <th scope='col'>Race</th>
              <th scope='col'>Gender</th>
              <th scope='col'>Actions</th>
            </tr>
          </thead>
          <tbody className='table-body'>
            {/*currentPosts.map((value, key) => {
              return (
                <tr>
                  <td>{key + 1 * page}</td>
                  <td>{value.name}</td>
                  <td>{value.race}</td>
                  <td>{value.gender}</td>
                  <button onClick={() => modalCharecterOpenFunction(value)}>Details</button>

                </tr>
              )
            })*/}
            {
              characterDataSourse =="" ?
                currentPosts.filter((value) => {
                  if (searchTerm == "") {
                    return value
                  }
                  else if (value.name.toLowerCase().includes(searchTerm.toLowerCase())) {
                    return value
                  }
                }).map((value, key) => {
                  return (
                    <tr>
                      <td>{(key + 1) + (page * postPerPage) - postPerPage}</td>
                      <td>{value.name}</td>
                      <td>{value.race}</td>
                      <td>{value.gender}</td>
                      <button onClick={() => modalCharecterOpenFunction(value)} className="details-btn">Details</button>

                    </tr>
                  )
                }) :
                <p className='no-data-found'>No data found</p>
            }
          </tbody>

        </table>
        {/** 
        <div className='pagination-div'>
          <Pagination postPerPage={postPerPage} totalPosts={characterDataSourse.length} paginate={paginate} />
          <select className='selectpagination' onChange={pageSizeFunc}>
            <option>Select Page size</option>
            <option value={10}>10</option>
            <option value={20}>20</option>
            <option value={50}>50</option>
          </select>
        </div> 
        */}

        <Pagination
          onChange={(value) => setPage(value)}
          pageSize={postPerPage}
          total={characterDataSourse.length}
          current={page}
          showSizeChanger
          showQuickJumper
          onShowSizeChange={onShowSizeChange}
          itemRender={itemRender}
        />

      </div>

    </div>
  );
}

/**
 *  <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
 */

export default App;
