//BaseURL
export const BASE_URL = 'https://the-one-api.dev/v2';

//SubURLs
export const CHARECTERS_URL = BASE_URL + '/character';